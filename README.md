Dans le cadre de la formation CDA, il nous a été demandé de réaliser la conception d'une application de gestion de budget à usage personnel.

J'ai donc commencé par réaliser un useCase via l'extension drawio.
J'ai séparé les fonctionnalités obligatoires et optionnelles.
Afin d'identifier au plus juste mes fonctionnalités, j'ai ciblé les mots clés de l'énoncé.


Pour la suite de ma conception, j'ai réalisé un diagramme de class dédiée à mes entités.
J'ai défini 5 classes, le USER, l'OPÉRATION, le SOLDE, le BUDGET, la CATEGORIE.

